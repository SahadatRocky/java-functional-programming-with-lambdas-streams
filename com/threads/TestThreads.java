package com.threads;

public class TestThreads {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<=1000; i++){
                    System.out.println(Thread.currentThread().threadId() + " " + i);
                }
            }
        };

        Runnable runnable1 = () -> {
            for(int i=0;i<=1000; i++){
                System.out.println(Thread.currentThread().threadId() + " " + i);
            }
        };

        Thread thread1 = new Thread(runnable1);
        thread1.run();
        Thread thread2 = new Thread(runnable1);
        thread2.run();
        Thread thread3 = new Thread(runnable1);
        thread3.run();
    }
}
