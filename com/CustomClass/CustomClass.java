package com.CustomClass;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class Course{
    private String name;
    private String category;
    private int reviewScore;
    private int numberOfStudent;

    public Course(String name, String category, int reviewScore, int numberOfStudent) {
        this.name = name;
        this.category = category;
        this.reviewScore = reviewScore;
        this.numberOfStudent = numberOfStudent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getReviewScore() {
        return reviewScore;
    }

    public void setReviewScore(int reviewScore) {
        this.reviewScore = reviewScore;
    }

    public int getNumberOfStudent() {
        return numberOfStudent;
    }

    public void setNumberOfStudent(int numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }

    @Override
    public String toString() {
        return "Course{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", reviewScore=" + reviewScore +
                ", numberOfStudent=" + numberOfStudent +
                '}';
    }
}


public class CustomClass {
    public static void main(String[]args){

        List<Course> courses = List.of(
                new Course("Spring", "Framework",98, 20000),
                new Course("Spring Boot", "Framework",95, 17000),
                new Course("API", "MicroServices",97, 22000),
                new Course("MicroServices", "MicroServices",96, 26000),
                new Course("FullStack", "FullStack",91, 14000),
                new Course("AWS", "Cloud",92, 21000),
                new Course("Azure", "Cloud",99, 22000),
                new Course("Docker", "Cloud",92, 20000),
                new Course("Kubernetes", "Cloud",91, 20000)
        );

        List<String> AllCourses = List.of("Spring", "Spring Boot","API","MicroServices", "FullStack", "AWS",  "Azure", "Docker", "Kubernetes");
        List<String> AllCourses2 = List.of("Sp", "SBoot","AP","Micro", "Full", "A",  "Azu", "Doc", "Kuber");


        //allMatch, noneMatch, anyMatch
        //System.out.println(courses.stream().allMatch(course -> course.getReviewScore() > 90));
        //System.out.println(courses.stream().noneMatch(course -> course.getReviewScore() > 90));
        //System.out.println(courses.stream().anyMatch(course -> course.getReviewScore() > 90));
        //System.out.println(courses.stream().sorted(Comparator.comparing(Course::getNumberOfStudent)).collect(Collectors.toList()));
        //System.out.println(courses.stream().sorted(Comparator.comparing(Course::getNumberOfStudent).reversed()).collect(Collectors.toList()));
        //System.out.println(courses.stream().sorted(Comparator.comparing(course -> course.getReviewScore() > 95)).limit(3).collect(Collectors.toList()));
        //System.out.println(courses.stream().sorted(Comparator.comparing(course -> course.getReviewScore() < 95)).skip(2).limit(3).collect(Collectors.toList()));
        //System.out.println(courses.stream().takeWhile(course -> course.getReviewScore() >= 95).collect(Collectors.toList()));
        System.out.println(courses.stream().max(Comparator.comparing(course -> course.getNumberOfStudent())));
        System.out.println(courses.stream().min(Comparator.comparing(course -> course.getNumberOfStudent())));
        System.out.println(courses.stream().findFirst().get());
        System.out.println(courses.stream().findAny().get());
        System.out.println(courses.stream().mapToInt(Course::getNumberOfStudent).sum());
        System.out.println(courses.stream().mapToInt(Course::getNumberOfStudent).average());
        System.out.println(courses.stream().mapToInt(Course::getNumberOfStudent).count());
        System.out.println(
                courses.stream()
                        .collect(Collectors.groupingBy(Course::getCategory))
        );

        System.out.println(
                courses.stream()
                        .collect(Collectors.groupingBy(Course::getCategory, Collectors.counting()))
        );

        System.out.println(
                AllCourses.stream().collect(Collectors.joining(" "))
        );
        System.out.println(
                AllCourses.stream().collect(Collectors.joining(","))
        );

        System.out.println(
                AllCourses.stream().flatMap(course1 -> AllCourses2.stream().map(course2 ->List.of(course1,course2))).collect(Collectors.toList())
        );

        List<String> modifiable = new ArrayList<>(AllCourses);

        modifiable.replaceAll(str -> str.toUpperCase());
        System.out.println(modifiable);
        modifiable.removeIf(course -> course.length() > 6);
        System.out.println(modifiable);

    }
}

