package com.functional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Functional {

    public static void main(String[] args) {
        List<Integer> numbers = List.of(12,9,13,4,6,2,4,12,15);
//        printAllNumberInListFunctional(numbers);
        printAllEvenNumberInListFunctional(numbers);
        int  sum = printAllNumberInListSumFunctional(numbers);
        System.out.println("Sum : " +sum);

       // printDistinictNumberInListFunctional(numbers);
        printDistinictSortedNumberInListFunctional(numbers);

       List<Integer> doubleNumber =  printAllDoubleInListFunctional(numbers);
        System.out.println("doubleNumber : " +doubleNumber);
    }

    private static void print(Integer number){
        System.out.println(number);
    }

    private static void printAllNumberInListFunctional(List<Integer> numbers) {
        numbers.stream().forEach(Functional::print);

        //   for (Integer number : integers){
        //      System.out.println(number);
        //   }
    }

    private static boolean isEven(Integer number){
         return number % 2 == 0;
    }
    private static void printAllEvenNumberInListFunctional(List<Integer> numbers) {
        numbers.stream()
                 //.filter(Functional::isEven)
                 .filter(number -> number % 2 == 0)  // lambda expression
                 .map(number -> number * number)
                 .forEach(Functional::print);

//        for (Integer number : integers){
//            if(number % 2 == 0){
//                System.out.println(number);
//            }
//        }
    }

    private static int printAllNumberInListSumFunctional(List<Integer> numbers){
       return numbers.stream().reduce(0,(x,y)-> x+y);
    }

    private static void  printDistinictNumberInListFunctional(List<Integer> numbers){
        numbers.stream().distinct().forEach(System.out::println);
    }

    private static void  printDistinictSortedNumberInListFunctional(List<Integer> numbers){
        numbers.stream().distinct().sorted().forEach(System.out::println);
    }

    private static List<Integer> printAllDoubleInListFunctional(List<Integer> numbers){
         return numbers.stream().map(number -> number * number).collect(Collectors.toList());
    }

}
