package com.functional;

import java.util.Comparator;
import java.util.List;

public class Exercise {
    public static void main(String[] args) {
        List<String> courses = List.of("Spring","Spring Boot", "AWS", "PCF", "Azure","API","Docker","Microservices", "Kubernetes");

//        courses.stream().filter(course -> course.contains("Spring"))
//                .forEach(System.out::println);


        //courses.stream().sorted(Comparator.naturalOrder()).forEach(System.out::println);
        //courses.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);
        courses.stream().sorted(Comparator.comparing(str -> str.length())).forEach(System.out::println);

    }
}
